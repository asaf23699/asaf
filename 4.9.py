__author__ = "Asaf"

import socket
import re
import os
IP = "0.0.0.0" #the ip
PORT = 80 #the used port
NORMAL_SIZE = 1024 #the data size that can be received
ROOT = "C:\wwwroot" #the folder root
ERROR_DIC = {404: "404 Not Found", #404 error when the file doesnt exist
             500: "500 Internal Server Error", #500 error when the packet that wa send to the server is not right
             403: "403 Forbidden", #403 error when the file asked for is closed by rhe server and forbidden
             302: "Moved Temporarily"} #302 error when the file asked by the user moved to a different path
FORBIDDEN = [ROOT + r"\forbid1.txt", ROOT + r"\forbid2.txt"] #the forbidden files list
MOVED = {ROOT + "\move1.txt": "/move2.txt", #the dictionary of files asked by the user that moved,
         ROOT + "\move3.txt": "/move4.txt"} #and their new path

FIBO_STRUCT = r'fibonacci\?element-num=(\d+)' #Added by Shahar


def build_packet(code, data_type, data):
    """
    SUMMARY: build builds an http packet with entered packets.
    GET: the code of the packet, the data type and the data itself
    RETURNS: the all packet
    """
    packet = 'HTTP/1.1 '+code+'\r\n' \
        'Content-Type: '+data_type+'\r\n' \
        'Content-Length = ' + str(len(str(data))) + '\r\n\r\n' + data
    return packet


def calculate(path):
    """
    SUMMARY: the function takes the file name and returns the number requested bigger by 1
    GET: the file requested by the user knowingly that the number seperated by "=" need to be increased by 1 and returned
    RETURNS: the num at the end plus 1
    """
    num_to_calcu = int(path.split("=")[len(path.split("="))-1]) #seperates the number from the path requested
    num_to_calcu += 1 #increases it by 1
    return str(num_to_calcu) #return the new number


def calculate_area(path):
    """
    SUMMARY: the function take the file_name and takes the number parameters to calculate the area and
             returnees it
    GET: the file name
    RETURNS: the triangle area that the numbers that were entered are his height and width
    """
    parameters = path.split("?")[1].split("&")
    height = float(parameters[0].split("=")[1])
    width = float(parameters[1].split("=")[1])
    area = float((height * width)/2)
    return str(area)


def file_read(path):
    """
    SUMMARY: the function takes the path and returns the copy of the file in the path
    GET: file path
    RETURNS: the copy of the file path
    """
    file_send = ""
    with open(path, "r+") as file_to_send: #creating the file to read from him
        for letter in file_to_send:
            file_send += letter
    return file_send #returning the copied file to send its content


def second_func(data):
    """
    SUMMARY: the function takes the data, checks if the code is ok and takes the name of the file the
             user searched for and returns a fitting Error if it doesnt exist or not ok or the user
             called for the calculate func
    GET: the data send from the user to the server
    RETURNS: the full packet with a suitable answer
    """
    if re.search("GET /\S+ HTTP/\S+\r\n", data): #checking the code is good
        data_parts_list = re.split("\s", data, re.UNICODE)
        path = ROOT + data_parts_list[1].replace("/", r'\ ').replace(" ", "") #separating the file searched by the user to a plausible path

#The following 4 lines were written by Shahar
        match = re.search(FIBO_STRUCT, path)
        if match:
            fibo_num = fibonacci(match.group(1))    #each match is a parameter to
                                                                                    # calculate the Fibonacci series
            return build_packet(code="20 OK", data_type="text", data=fibo_num)      #builds the packet

        if "calculate-area?height=" and "&width=" in path: #checks if the user calls the calculate area func
            area = calculate_area(path) #calls the calculate area func that calculates the area
            return build_packet(code="20 OK", data_type="text", data=area) #builds the packet
        if "calculate-next" in path: #checks if the user called for the calculate func or for the number "5"
            if "?num=" in path:
                num_to_calculate = calculate(path) #calles for the calculate func
                return build_packet(code="200 Ok", data_type="text", data=num_to_calculate) #builds the packet
            else:
                return build_packet(code="200 Ok", data_type="text", data="5") #builds the packet with the number "5"
        if path in FORBIDDEN:
            return build_packet(code=ERROR_DIC[403], data_type="text", data=ERROR_DIC[403])#creating the 403 error packet
        else:
            if path in MOVED:
                return build_packet(code=ERROR_DIC[302], data_type="text", data=ERROR_DIC[302] + " try:" + MOVED[path])
                #creating the 302 error packet
            else:
                if os.path.isfile(path): #checking if there is a vaild file and if it is not it writes an Error
                    file_send = file_read(path) #copies the path given to the func
                    if ".html" in path:
                        data_to_send = build_packet(code="200 OK", data_type="html", data=file_send) #creating an html packet
                    else:
                        data_to_send = build_packet(code="200 OK", data_type="text", data=file_send)#creating  a text packet
                    return data_to_send
                else:
                    if path == ROOT + "\wwwroot": #if the user try ro get the "wwwroot" folder
                        index_data = file_read(ROOT + "\index.html")
                        return build_packet(code="200 OK", data_type="html", data=index_data) #creating the index packet
                    else:
                        return build_packet(code=ERROR_DIC[404], data_type="text", data=ERROR_DIC[404]) #creating the 404 error packet
    else:
        return build_packet(code=ERROR_DIC[500], data_type="text", data=ERROR_DIC[500]) #creating the 500 error packet


def fibonacci(element_num):
    """
    SUMMARY: This is an one liner for a specified element in the Fibonacci series.
    GET: element_num - the serial number of the element that the user wish to calculate
    RETURN: The asked element
    """
    if element_num != 0:
        return str(reduce(lambda x, y: (x[0]+x[1], x[0]), [(0, 1)]*int(element_num))[0])
    else:
        return "Not Possible"


if __name__ == "__main__":
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((IP, PORT))
    while True:
        server_socket.listen(1)
        client_socket, address = server_socket.accept()
        data = client_socket.recv(NORMAL_SIZE)
        data_received = second_func(data)
        client_socket.send(data_received)
        client_socket.close()
    server_socket.close()